#!/usr/bin/env bash
depends() {
    echo "installing dependency of ${PACKAGE_NAME}"
case "${PACKAGE_NAME}" in
    "kodi")
    sed -i "84a [archlinuxcn]\nServer = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/\$arch\n" /etc/pacman.conf
    pacman-key --init
    pacman-key --lsign-key "farseerfc@archlinux.org"
    pacman -Sy archlinuxcn-keyring --noconfirm
    pacman -S mysql --noconfirm
    return
    ;;
esac
}

url() {
case "${REPO}" in
    "archlinux")
    echo "https://gitlab.archlinux.org/archlinux/packaging/packages/${PACKAGE_NAME}.git"
    return
    ;;
    "aur")
    echo "https://aur.archlinux.org/${PACKAGE_NAME}.git"
    return
    ;;
    *)
    echo "not support for getting ${REPO} repo url yet."
    exit 1
    ;;
esac
}

pacman -Sy git --noconfirm
depends
git clone --depth=1 $(url) ${PACKAGE_NAME}
